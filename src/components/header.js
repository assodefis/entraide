import React from "react"
import { Navbar, Nav, Row, Col } from "react-bootstrap"
import { withPrefix } from "../../.cache/gatsby-browser-entry"
import Language from "./language"
import { Link } from "gatsby-plugin-intl"

const Header = ({ siteTitle, description, moreInfo }) => (
  <>
    <Navbar bg="transparent" expand="lg">
      <Nav className="mr-auto" />
      <Language />
    </Navbar>
    <header>
      <Row style={{ color: "#666" }} className="justify-content-md-center">
        <Col xs sm="12" md="10" lg="8">
          <div style={{ maxWidth: "670px", margin: "0 auto" }}>
            <Link to="/">
              <img
                src={withPrefix(`/pictures/logo_chatons_v2.1.svg`)}
                alt="CHATONS"
              />
            </Link>
            <div>
              <Link to="/">
                <h1>{siteTitle}</h1>
              </Link>
              <p>{description}</p>
              <Link className="link-more-info" to="/about">
                {moreInfo}
              </Link>
            </div>
          </div>
        </Col>
      </Row>
    </header>
  </>
)

export default Header
