---
title: "About"
date: "2020-03-31T12:30:03.284Z"
---

_Following the quarantine due to the COVID-19 outbreak, many of you need tools to continue communicating with your loved ones and/or working from home. The Collective of Independent, Transparent, Open, Neutral and United Hosters - CHATONS - is setting up this portal to simplify access to online services that meet the most common needs with no registration required._

All of these services are offered by CHATONS hosters who commit to respecting [the charter of the collective](https://chatons.org/en/charter). Initiated in 2016 by Framasoft, this collective currently brings together more than 70 structures that offer free, ethical, decentralized and solidarity-based online services to enable Internet users to quickly find alternatives to the services offered by the web giants.

We plan to maintain this page beyond the quarantine time and even to add more content in the coming weeks / months in order to eventually give you access to other free tools hosted by the CHATONS, including those that require registration.
