---
title: "About"
date: "2020-03-31T12:30:03.284Z"
---

_Following the containment due to the COVID-19 outbreak, many of you need tools to continue communicating with your loved ones and/or teleworking. The Collective of independent, transparent, open, neutral and ethical hosters - CHATONS - is setting up this portal for simplified access to online services without registration and meeting the most common needs._

All these services are offered by CHATONS hosts who undertake to respect [the charter of the collective](https://chatons.org/en/charter). Initiated in 2016 by Framasoft, this collective currently brings together more than 70 structures that offer free, ethical, decentralized and solidarity-based online services to enable Internet users to quickly find alternatives to the services offered by the web giants.

We plan to maintain this page beyond the confinement time and even to enrich it in the coming weeks / months in order to eventually offer you other free tools proposed by the CHATONS members, including those requiring registration.
